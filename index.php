<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>S04: Access Modifiers and Encapsulation</title>
	</head>
	<body>
		<h1>Access Modifiers</h1>

		<h2>Building Object</h2>
		<p><?php //echo $building->name; ?></p>

		<h2>Condominium Object</h2>
		<p><?php //echo $condominium->name; ?></p>

		<h1>Encapsulation</h1>
		<p>The name of the condominium is <?= $condominium->getName(); ?></p>

		<?php $condominium->setName("Enzo Tower"); ?>
		<p>The name of the condominium is <?= $condominium->getName(); ?></p>

		<p><?php //var_dump($building); ?></p>
		<p><?php //var_dump($condominium); ?></p>

<!-- ---------------------------------------------------------------------------------------- -->
		<?php $condominium->setName("Enzo Condo"); ?>
		<h1>Building</h1>
		<!-- Activity codes here -->
		<p>The name of the building is <?= $building->getName(); ?></p>
		<p>The <?= $building->getName(); ?> has <?= $building->getFloor(); ?> floors.</p>
		<p>The <?=$building->getName(); ?> is located at <?= $building->getAddress(); ?>.</p>
		<?php $building->setName("Caswynn Complex"); ?>
		<p>The name of the building has been changed to <?= $building->getName(); ?>.</p>

		<h1>Condominium</h1>
		<!-- Activity codes here -->
		<p>The name of the building is <?= $condominium->getName(); ?></p>
		<p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloor(); ?> floors.</p>
		<p>The <?=$condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?>.</p>
		<?php $condominium->setName("Enzo Tower"); ?>
		<p>The name of the condominium has been changed to <?= $condominium->getName(); ?>.</p>

	</body>
</html>